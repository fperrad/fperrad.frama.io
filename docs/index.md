
# Web Summary

---

# Main Links

- my [LinkedIn](http://www.linkedin.com/in/fperrad) profile
- my [OpenHUB](https://www.openhub.net/accounts/fperrad) profile
- my [Framagit](https://framagit.org/fperrad/) account

# Articles

- [DTD+RE](https://web.archive.org/web/20080422020934/http://lists.dsdl.org/dsdl-discuss/2003-12/att-0003/dtd-regex_en.pdf) :
  an extension of the [XML W3C](http://www.w3.org/TR/2000/REC-xml-20001006) recommendation with Regular Expression
- [version française](https://web.archive.org/web/20191229122233/http://xmlfr.org/documentations/articles/030729-0001) publiée sur [XMLfr](http://xmlfr.org/)
- [L'expansion de Perl dans les Makefiles](res/linuxmag138.jpeg)
  @ GNU/Linux Magazine N°138 (2011-05)
- [La Sortie de LuaJIT 2.0.0](http://linuxfr.org/news/sortie-de-luajit-2-0-0)
  @ [linuxfr.org](http://linuxfr.org/)

# Talks

- [Lua on Parrot](res/LuaOnParrot.pdf)
  @ [French Perl Workshop 2007](http://conferences.mongueurs.net/fpw2007/)
- [CORBA::XS another way between Perl and C](res/CORBA-XS.pdf)
  @ [French Perl Workshop 2008](http://conferences.mongueurs.net/fpw2008/)
- [Lua Regex on Parrot](res/LuaRegexOnParrot.pdf)
  @ [French Perl Workshop 2008](http://conferences.mongueurs.net/fpw2008/)
- [Markdown on Parrot](res/MarkdownOnParrot.pdf)
  @ [French Perl Workshop 2009](http://conferences.mongueurs.nçet/fpw2009/)
- [Backpan to Github](res/Backpan2Github.pdf)
  @ [French Perl Workshop 2009](http://conferences.mongueurs.net/fpw2009/)
- [The Parrot VM](res/parrot.pdf)
  @ [OSDC.fr 2009](http://act.osdc.fr/osdc2009fr/)
- [The State Machine Compiler](res/smc.pdf)
  @ [OSDC.fr 2009](http://act.osdc.fr/osdc2009fr/)
- [A Toochain for the Parrot Ecosystem](res/Toolchain.pdf)
  @ [French Perl Workshop 2010](http://conferences.mongueurs.net/fpw2010/)
- [PIR Samples](res/PirSamples.pdf)
  @ [French Perl Workshop 2010](http://conferences.mongueurs.net/fpw2010/)
- [Lua and its ecosystem](res/lua_ecosystem.pdf)
  @ [OSDC.fr 2011](http://act.osdc.fr/osdc2011fr/)
- [LuaJIT](res/luajit.pdf)
  @ [OSDC.fr 2011](http://act.osdc.fr/osdc2011fr/)
- [TDD in deeply embedded system (Arduino) with TAP](res/ArduinoTap.pdf)
  @ [French Perl Workshop 2012](http://conferences.mongueurs.net/fpw2012/)
- [Parrot Statistics](res/ParrotStatistics.pdf)
  @ [French Perl Workshop 2012](http://conferences.mongueurs.net/fpw2012/)
- [YAP6I](res/YAP6I.pdf)
  @ [French Perl Workshop 2013](http://conferences.mongueurs.net/fpw2013/)
- [Travis CI for Perl](res/fperrad/TravisCI.pdf)
  @ [French Perl Workshop 2013]()
- [Perl on embedded Linux with BuildRoot](res/buildroot.pdf)
  @ [French Perl Workshop 2014](http://conferences.mongueurs.net/fpw2014/)

# Open Source Contributions

## [CPAN](http://www.cpan.org/) : the Comprehensive Perl Archive Network

Perl5 Module Author since 2001 :

- [Chart-Plot-Canvas](https://metacpan.org/release/Chart-Plot-Canvas/) : Plot two dimensional data in an Tk Canvas.
- [Chart-Plot-Tagged](https://metacpan.org/release/Chart-Plot-Tagged/) : Plot with tags
- [CORBA-C](https://metacpan.org/release/CORBA-C/) : IDL compiler to language C mapping
- [CORBA-Cplusplus](https://metacpan.org/release/CORBA-Cplusplus/) : IDL compiler to language C++ mapping
- [CORBA-HTML](https://metacpan.org/release/CORBA-HTML/) : HTML documentation generator from IDL source
- [CORBA-IDL](https://metacpan.org/release/CORBA-IDL/) : Parsers for the language CORBA IDL
- [CORBA-JAVA](https://metacpan.org/release/CORBA-JAVA/) : IDL compiler to language JAVA mapping
- [CORBA-Perl](https://metacpan.org/release/CORBA-Perl/) : IDL compiler to language Perl mapping
- [CORBA-Python](https://metacpan.org/release/CORBA-Python/) : IDL compiler to language Python mapping
- [CORBA-XMLSchemas](https://metacpan.org/release/CORBA-XMLSchemas/) : IDL compiler to WSDL/SOAP and W3C Schema
    Issues posted to [OMG](http://www.omg.org/) about the [CORBA to WSDL/SOAP Interworking Specification](http://www.omg.org/technology/documents/formal/CORBA_WSDL.htm) :
       - [6631](http://www.omg.org/issues/issue6631.txt)
       - [6634](http://www.omg.org/issues/issue6634.txt)
       - [8755](http://www.omg.org/issues/issue8755.txt)
       - [8806](http://www.omg.org/issues/issue8806.txt)
       - [11687](http://www.omg.org/issues/issue11687.txt)
- [CORBA-XPIDL](https://metacpan.org/release/CORBA-XPIDL/) : XPIDL compiler (for Mozilla)
- [CORBA-XS](https://metacpan.org/release/CORBA-XS-0.61/) : IDL compiler to extension between Perl & C
- [CVS-Metrics](https://metacpan.org/release/CVS-Metrics/) : Utilities for process cvs log
- [DFA-Statemap](https://metacpan.org/release/DFA-Statemap/) : Runtime library for [SMC](http://smc.sourceforge.net/)
- [Pod-Simple-Data](https://metacpan.org/release/Pod-Simple-Data/) : retrieve the data inlined in Pod
- [re-engine-Lua](https://metacpan.org/release/re-engine-lua/) : [Lua](http://www.lua.org/) regular expression engine plugin for Perl 5.10
- [re-engine-LPEG](https://metacpan.org/release/re-engine-lpeg/) : [LPeg](http://www.lua.org/) regular expression engine plugin for Perl 5.10
- [Tk-FilterEntry](https://metacpan.org/release/Tk-FilterEntry/) : An entry with filter
- [WAP-SAXDriver-wbxml](https://metacpan.org/release/WAP-SAXDriver-wbxml/) : SAX parser for WBXML file
- [WAP-wbxml](https://metacpan.org/release/WAP-wbxml/) : Binarization of XML file
- [WAP-wmls](https://metacpan.org/release/WAP-wmls/) : WAP WMLScript compiler
- [XML-Handler-Dtd2DocBook](https://metacpan.org/release/XML-Handler-Dtd2DocBook-0.41/) : SAX2 handler for generate a DocBook documentation from a DTD
- [XML-Handler-Dtd2Html](https://metacpan.org/release/XML-Handler-Dtd2Html/) : SAX2 handler for generate a HTML documentation from a DTD

and few patches in the Perl5 core, [here](https://www.openhub.net/p/perl/commits?query=perrad) and [here](https://www.openhub.net/p/8110/commits?query=perrad). 

## [SMC](http://smc.sourceforge.net/) : the State Machine Compiler

author/maintainer of the following target languages :

- [Perl](http://www.perl.org/)
- [Python](http://www.python.org/)
- [Ruby](http://www.ruby-lang.org/en/)
- C
- [Groovy](http://www.groovy-lang.org/)
- [Lua](http://www.lua.org/)
- [Scala](http://www.scala-lang.org/)

## [Parrot](http://www.parrot.org/) : the Perl6 Virtual Machine

parrot-porter since 2005 :

- responsible for the [MinGW](http://www.mingw.org/) port (gcc on Windows)
- author/maintainer of the [Lua](http://github.com/fperrad/lua/) language
- author/maintainer of the [WMLScript](http://github.com/fperrad/wmlscript/) bytecode translator
- author/maintainer of the [Markdown](http://github.com/fperrad/markdown/) language

## [parrotwin32](http://sourceforge.net/projects/parrotwin32/) : the repository of the binairies for Windows

owner

## [Lua](http://www.lua.org/) : a scripting language

ports of some great Perl5 modules

- [lua-ChartPlot](https://fperrad.frama.io/lua-chartplot/) : plot two dimensional data in an image
- [lua-Coat](https://fperrad.frama.io/lua-Coat/) : Yet Another Lua Oriented Object Model
- [lua-CoatPersistent](https://fperrad.frama.io/lua-CoatPersistent/) : an ORM
- [lua-TestMore](https://fperrad.frama.io/lua-TestMore/) : an Unit Testing Framework
- [lua-TestLongString](https://fperrad.frama.io/lua-TestLongString/) : an extension for testing long string

and some original modules

- [lua-CodeGen](https://fperrad.frama.io/lua-CodeGen/) : a template engine
- [lua-ConciseSerialization](https://fperrad.frama.io/lua-ConciseSerialization/) : a pure Lua implementation of [CBOR](http://cbor.io/) ([RFC 8949](http://tools.ietf.org/html/rfc8949))
- [lua-LIVR](https://fperrad.frama.io/lua-LIVR/) : Lua implementation of [Language Independent Validation Rules](http://livr-spec.org>)
- [lua-LIVR-extra](https://fperrad.frama.io/lua-LIVR-extra/) : more [LIVR](http://livr-spec.org) rules
- [lua-MessagePack](https://fperrad.frama.io/lua-MessagePack/) : a pure Lua implementation of [MessagePack](http://msgpack.org/) serialization
- [lua-Rotas](https://fperrad.frama.io/lua-Rotas/) : a web server router
- [lua-Silva](https://fperrad.frama.io/lua-Silva/) : your personal string matching expert
- [lua-Spore](https://fperrad.frama.io/lua-Spore/) : a generic [ReST](http://en.wikipedia.org/wiki/Representational_State_Transfer) client
- [lua-TestAssertion](https://fperrad.frama.io/lua-testassertion/) : a friendly extension of [lua-TestMore](https://fperrad.frama.io/lua-TestMore/)
- [lua-csnappy](https://fperrad.frama.io/lua-csnappy/) : a binding of [Snappy](https://google.github.io/snappy/) (de)compressor
- [lua-mqtt](https://fperrad.frama.io/lua-mqtt/) :  a client library for [MQTT](https://mqtt.org/) 3.1.1 & 5
- [lua-ubjson](https://fperrad.frama.io/lua-ubjson/) :  a pure Lua implementation of [UBJSON](https://ubjson.org/) 
- [ljlinenoise](https://fperrad.frama.io/ljlinenoise) : Line editing in pure [LuaJIT](http://luajit.org/)

## [Arduino](http://www.arduino.cc/) : the Open-Source embedded platform

- [ArduinoTap](https://framagit.org/fperrad/ArduinoTap) : a TAP ([Test Anything Protocol](http://en.wikipedia.org/wiki/Test_Anything_Protocol)) producer library
- [tap4embedded](https://framagit.org/fperrad/tap4embedded) : a [TAP](http://en.wikipedia.org/wiki/Test_Anything_Protocol) producer library designed for C embedded

## [BuildRoot](http://www.buildroot.net/) : making Embedded Linux easy

see my contributions, [here](https://www.openhub.net/p/buildroot/commits?query=perrad).

## Miscellaneous :

Versions in timeline :
[linux](https://fperrad.frama.io/graph-versions/linux.html),
[git](https://fperrad.frama.io/graph-versions/scm.html#git),
[gcc](https://fperrad.frama.io/graph-versions/tools.html#gcc),
[perl](https://fperrad.frama.io/graph-versions/perl.html),
[lua](https://fperrad.frama.io/graph-versions/interp.html#lua)

## Patents

- [EP0974937](http://www.freepatentsonline.com/EP0974937.html) - Printing and recording module for a transport ticket
- [EP0978804](http://www.freepatentsonline.com/EP0978804.html) - Output module for a ticket producing and delivering apparatus, in particular for transport tickets

---

Contact : francois.perrad@gadz.org
